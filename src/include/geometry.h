#pragma once 

#include "quadtree.h"
#include "recupimage.h"
#include <GL/gl.h>

typedef struct Vect2i {
    int x;
    int y;
} Vect2i;

typedef struct Vect2f {
    float x;
    float y;
} Vect2f;

typedef struct Vect3f {
    float x;
    float y;
    float z;
} Vect3f;

typedef struct Triangle {
    Vect2f p1;
    Vect2f p2;
    Vect2f p3;
} Triangle;

typedef struct Triangle3D {
    Vect3f p1;
    Vect3f p2;
    Vect3f p3;
} Triangle3D;


Vect3f createVector(float x, float y, float z);

Vect2f addVectors(Vect2f v1, Vect2f v2);
Vect2f subVectors(Vect2f v1, Vect2f v2);
Vect2f multVectors(Vect2f v1, Vect2f v2);
Vect2f divVectors(Vect2f v1, Vect2f v2);

Vect3f addVectors(Vect3f v1, Vect3f v2);
Vect3f subVectors(Vect3f v1, Vect3f v2);
Vect3f multVector(Vect3f v, float a);
Vect3f divVector(Vect3f v, float a);

float dot2D(Vect2f v1, Vect2f v2);
float dot3D(Vect3f v1, Vect3f v2);

float norm(Vect3f v);
float normSq(Vect3f v);
Vect3f normalize(Vect3f v);
Vect3f cross(Vect3f v1, Vect3f v2);

Vect2f rotation(Vect2f v, float angle);
Vect3f rotation_soleil(float angle);

float sign(float x);
float clamp(float x, float m, float M);
float distance(Vect3f p1, Vect3f p2);
float dist_to_triangle(Triangle3D triangle, Vect3f p);

Vect2f world2pixels2D(Vect2f v, Vect2f image_size, Vect2f world_size);
Vect3f world2pixels3D(Vect3f v, Vect2f image_size, Vect2f world_size);
Vect3f pixels2world(Vect3f v, Vect2f image_size, Vect2f world_size);

bool IsPointInSquare(Vect2f point, Square& square);
bool IsRightSide(Vect2f point, Vect2f p1, Vect2f p2);
bool IsPointInTriangle(Vect2f point, Triangle& triangle);

bool segmentIntersection(Vect2f p1, Vect2f p2, Vect2f q1, Vect2f q2);
bool intersection(Triangle& triangle, Square& square);

float conversionValue(float oldVal, float oldMax, float oldMin, float newMax, float newMin);
float getHeight(int x, int y, int zmin, int zmax, Image* image);
float getHeightNormalise(int x, int y, Image* image);

void drawSquare(Square square, Image* image, int zmin, int zmax, GLuint texture, bool filled, Vect3f direction_soleil);
void drawTerrain(Node* node, Triangle cameraFrustum, Vect3f positionCam, Image* image, int zmin, int zmax, GLuint texture, bool filled,  Vect3f direction_soleil, Vect2f image_size, Vect2f world_size);
void drawObjet(GLuint texture_arbre,int zmin, int zmax,Image* image,float pan,float x,float y,float obj_hauteur, Vect3f direction_soleil);
