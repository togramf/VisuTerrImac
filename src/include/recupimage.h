#ifndef RECUPIMAGE_H
#define RECUPIMAGE_H

#include <vector>

typedef struct Image {
    int w,h;
    int dyn;
    std::vector<int> dat;
} Image;

int recupParamMap(char* argv, char* path, float* xsize, float* ysize, float* zmin, float* zmax, float* znear, float* zfar, float* fov);
Image* newImage(int w, int h, int d);
int getPixel (Image* I, int i, int j);
Image* loadImage (char* fichier);

#endif
