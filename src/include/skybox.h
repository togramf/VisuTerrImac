#include <GL/gl.h>

struct Img {
    int width;
    int height;
    unsigned char* data;
};

GLuint gen_skybox();
void render_skybox(GLuint cube_map_texture_ID);