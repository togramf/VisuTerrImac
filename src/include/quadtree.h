#pragma once

typedef struct Square {
    int x1;
    int x2;
    int y1;
    int y2;
} Square;

typedef struct Node {
    Square square;
    Node *hautg=nullptr;
    Node *hautd=nullptr;
    Node *basg=nullptr;
    Node *basd=nullptr;
} Node;

void addNode(Node* node, int x1, int x2, int y1, int y2);

Node* quadTree(int  size);
