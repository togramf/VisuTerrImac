#include <GL/gl.h>
#include <cmath>
#include <iostream>

#include "./include/quadtree.h"

void addNode(Node* node, int x1, int x2, int y1, int y2)
{
    node->square = { x1, x2, y1, y2 };
    
    int x=(x1+x2)/2;
    int y=(y1+y2)/2;

    if (x2-x1 > 1) {
        node->hautg=new Node();
        node->hautd=new Node();
        node->basg=new Node();
        node->basd=new Node();

        addNode(node->hautg,x1,x,y1,y);
        addNode(node->hautd,x,x2,y1,y);
        addNode(node->basg,x1,x,y,y2);
        addNode(node->basd,x,x2,y,y2);
    }
}

Node* quadTree(int  size)
{
    Node* node = new Node();
    addNode(node, 0, size, 0, size);
    return node;
}
