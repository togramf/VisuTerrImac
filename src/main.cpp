#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

#include "./include/skybox.h"
#include "./include/camera.h"
#include "./include/gen_texture.h"

static const unsigned int WINDOW_WIDTH = 800;
static const unsigned int WINDOW_HEIGHT = 800;
static const unsigned int BIT_PER_PIXEL = 32;
static const char* WINDOW_TITLE = "Jos'IMAC";

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 1.;
static float aspectRatio;

void onWindowResized(unsigned int width, unsigned int height, float fov, float zNear, float zFar)
{
    aspectRatio = width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, aspectRatio, zNear, zFar);
    glMatrixMode(GL_MODELVIEW);
}


int main(int argc, char*argv[]){

    /* Initialisation des variables à récupérer dans le .timac */
    char path_hm[40] = "";
    float xsize = 0, ysize = 0;
    float zmin = 0, zmax = 0, znear=0, zfar=0;
    float fov=0;

    if (!recupParamMap(argv[1], path_hm, &xsize, &ysize, &zmin, &zmax, &znear, &zfar, &fov))
        return 0;
    
    /* Vérification de l'initialisation des variables */
    printf("nom=%s\n", path_hm );
    printf("xsize=%f ysize=%f zmin=%f zmax=%f\n", xsize, ysize, zmin, zmax); // On affiche la chaîne 

    /* Récupération image pgm */
    Image* image = loadImage (path_hm);

    /* Récup QuadTree */
    Node* tree = quadTree (image->h);


    /* Initialisation de la SDL */

    if(SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        const char* error = SDL_GetError();
        fprintf(
            stderr, 
            "Erreur lors de l'intialisation de la SDL : %s\n", error);

        SDL_Quit();
        return EXIT_FAILURE;
    }

    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */

    SDL_Window* window;
    {
        window = SDL_CreateWindow(
        WINDOW_TITLE,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if(NULL == window) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation de la fenetre : %s\n", error);

            SDL_Quit();
            return EXIT_FAILURE;
        }
    }

    SDL_GLContext context;
    {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

            SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        context = SDL_GL_CreateContext(window);
    
        if(NULL == context) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation du contexte OpenGL : %s\n", error);

            SDL_DestroyWindow(window);
            SDL_Quit();
            return EXIT_FAILURE;
        }
    }

    onWindowResized(WINDOW_WIDTH, WINDOW_HEIGHT, fov, znear, zfar);

    /* Preparation & ajout d'objets dans la map */

    // Arbres
    constexpr int max_arbres=30;
    float arbres[max_arbres*2];
    float arbre_hauteur=7;
    int nb_arbres=0;
    for(int k=0;k<max_arbres*2;k+=2){
         float pos_x_arbre = std::rand()% (image->w-1 )+ 0;
         float pos_y_arbre = std::rand()% (image->h-1) + 0;
         arbres[k]=pos_x_arbre;
         arbres[k+1]=pos_y_arbre;
    }

    // Chèvre
    int nb_chevres=0;
    constexpr int max_chevres=1;
    float chevres[max_chevres*2];
    float chevre_hauteur=1;
    for(int k=0;k<max_chevres*2;k+=2){
         float pos_x_chevre = std::rand()% (image->w-1 )+ 0;
         float pos_y_chevre = std::rand()% (image->h-1) + 0;
         chevres[k]=pos_x_chevre;
         chevres[k+1]=pos_y_chevre;
    }

    // Pandas
    constexpr int max_pandas=10;
    float pandas[max_pandas*2];
    float panda_hauteur=2;
    int nb_pandas=0;
    for(int k=0;k<max_pandas*2;k+=2){
         float pos_x_panda= std::rand()% (image->w-1 )+ 0;
         float pos_y_panda = std::rand()% (image->h-1) + 0;
         pandas[k]=pos_x_panda;
         pandas[k+1]=pos_y_panda;
    }

    GLuint texture_sol = gen_texture("./doc/Texture/index.jpeg");
    GLuint texture_arbre = gen_texture("./doc/Texture/arbre.png");
    GLuint texture_chevre = gen_texture("./doc/Texture/chevre.png");
    GLuint texture_panda= gen_texture("./doc/Texture/panda.png");

    /* Génération Skybox */
    GLuint cube_map_texture_ID = gen_skybox();

    /* Paramètres */  
    bool affichage_rempli = true;
    bool mode_visualisation =false;

    Vect2f image_size = {image->w, image->h};
    Vect2f world_size = {xsize, ysize};

    Camera camera;

    // Soleil
    float angle_soleil = 1.04;
    Vect3f direction_soleil = rotation_soleil(angle_soleil);

    // Boucle
    bool loop = true;

    while(loop) {

        /* Dessin */
        glClearColor(1., 0., 0., 1.);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);

        // Skybox
        glLoadIdentity();
        camera.applique_rotation();
        render_skybox(cube_map_texture_ID);

        // Terrain
        glDisable(GL_LIGHTING);
        
        glLoadIdentity();
        camera.applique_rotation();
        camera.applique_translation();

        if (mode_visualisation){
            Vect2f cam_in_pixel = world2pixels2D({camera.position().x, camera.position().z},image_size,world_size);
            int x = cam_in_pixel.x;
            int y = cam_in_pixel.y;
            if(x>=0 && x<image_size.x && y>=0 && y<image_size.y)
                camera.hauteur_fixe(getHeight(x, y, zmin, zmax, image), 8.f);
        }

        glScalef(xsize / image->w, 1., ysize / image->h);
        glTranslatef(-image->w / 2.f, 0, -image->h / 2.f);
        Triangle frustum = camera.frustum(zfar, fov, image_size, world_size);
        Vect3f cam_position = camera.position();
        drawTerrain(tree, frustum, cam_position, image, zmin, zmax, texture_sol, affichage_rempli, direction_soleil, image_size, world_size);
        
        // Arbres
        for(int k=0;k<nb_arbres*2;k+=2){
            drawObjet(texture_arbre,zmin,zmax,image,camera.pan(),arbres[k],arbres[k+1],arbre_hauteur,direction_soleil);
        }
        // Chèvre
        for(int k=0;k<nb_chevres*2;k+=2){
            drawObjet(texture_chevre,zmin,zmax,image,camera.pan(),chevres[k],chevres[k+1],chevre_hauteur,direction_soleil);
        }
        // Pandas
        for(int k=0;k<nb_pandas*2;k+=2){
            drawObjet(texture_panda,zmin,zmax,image,camera.pan(),pandas[k],pandas[k+1],panda_hauteur,direction_soleil);
        }
        
        SDL_Event e;
        while(SDL_PollEvent(&e)) {
        
            switch(e.type) 
            {
                case SDL_QUIT:
                    loop = false;
                    break;

                /*Redimensionnement fenetre */  
                case SDL_WINDOWEVENT_RESIZED:
                    onWindowResized(e.window.data1, e.window.data2, fov, znear, zfar);
                    break; 

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    
                    switch(e.button.button)
                    {
                        case SDL_BUTTON_LEFT:
                         
                            break;

                        case SDL_BUTTON_RIGHT:
                            
                            break;

                        default:
                            break;
                    }
                    break;
                
                /* Touche clavier */
                case SDL_KEYDOWN:
                    if (e.key.keysym.sym == SDLK_ESCAPE){
                        // Echap : quit 
                        loop = 0;
                    } 

                    // Translations
                    else if (e.key.keysym.sym == SDLK_z){
                        camera.avance_tout_droit();
                    } 
                    else if (e.key.keysym.sym == SDLK_s){
                        camera.avance_en_arriere();
                    } 
                    else if (e.key.keysym.sym == SDLK_q){
                        camera.avance_gauche();
                    } 
                    else if (e.key.keysym.sym == SDLK_d){
                        camera.avance_droite();
                    }
                    else if (e.key.keysym.sym == SDLK_e){
                        if (!mode_visualisation)
                            camera.avance_haut();
                    } 
                    else if (e.key.keysym.sym == SDLK_SPACE){
                        if (!mode_visualisation)
                            camera.avance_bas();
                    }

                    // Rotations
                    else if (e.key.keysym.sym == SDLK_LEFT){
                        camera.tourne_gauche();
                    } 
                    else if (e.key.keysym.sym == SDLK_RIGHT){
                        camera.tourne_droite();
                    } 
                    else if (e.key.keysym.sym == SDLK_UP){
                        camera.tourne_haut();
                    } 
                    else if (e.key.keysym.sym == SDLK_DOWN){
                        camera.tourne_bas();
                    } 

                    // Visualisation
                    else if (e.key.keysym.sym == SDLK_v){
                        mode_visualisation = !mode_visualisation;
                    } 

                    // Mode fil de fer
                    else if (e.key.keysym.sym == SDLK_f){
                        affichage_rempli = !affichage_rempli;
                    } 

                    // Lumière du soleil
                    else if (e.key.keysym.sym == SDLK_l){
                        angle_soleil += 0.01;
                        direction_soleil = rotation_soleil(angle_soleil);
                    } 
                    else if (e.key.keysym.sym == SDLK_m){
                        angle_soleil -= 0.01;
                        direction_soleil = rotation_soleil(angle_soleil);
                    } 

                    // Retour au centre de la map
                    else if (e.key.keysym.sym == SDLK_r){
                        camera.reinitialise_position();
                    }
                    
                    //Objet
                    else if (e.key.keysym.sym == SDLK_1){
                           //ajout d'un arbre 
                            if (nb_arbres +1 <max_arbres)
                                nb_arbres ++;
                            

                    }else if (e.key.keysym.sym == SDLK_2){
                            //retrait d'un arbre 
                                if (nb_arbres > 0)
                                    nb_arbres --;
                    }
                    else if (e.key.keysym.sym == SDLK_3){
                            //ajout d'un panda
                                if (nb_pandas +1 <max_arbres)
                                    nb_pandas ++;
                                   
                    }else if (e.key.keysym.sym == SDLK_4){
                            //retrait d'un panda
                                    if (nb_pandas > 0)
                                        nb_pandas --;
                    }
                    else if (e.key.keysym.sym == SDLK_5){
                            //ajout de la chèvre
                                    if (nb_chevres +1 <max_chevres)
                                        nb_chevres ++;
                                    
                    }else if (e.key.keysym.sym == SDLK_6){
                            //retrait de la chèvre
                                    if (nb_chevres > 0)
                                        nb_chevres --;
                    }else if (e.key.keysym.sym == SDLK_7){
                            //ajout de tout
                                    nb_arbres=max_arbres;
                                    nb_chevres=max_chevres;
                                    nb_pandas=max_pandas;
                           
                    }else if (e.key.keysym.sym == SDLK_8){
                            //retrait de tout
                                    nb_arbres=0;
                                    nb_chevres=0;
                                    nb_pandas=0;
                    }

                    break;
                   
                default:
                    break;
            }
        }
        
        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapWindow(window);
        
    }

    /* Liberation de la memoire allouee sur le GPU pour la texture */
    glDeleteTextures(1, &texture_sol);
    glDeleteTextures(1, &texture_arbre);
    glDeleteTextures(1, &texture_chevre);
    glDeleteTextures(1, &texture_panda);

    SDL_Quit();
    
    return EXIT_SUCCESS;
}
