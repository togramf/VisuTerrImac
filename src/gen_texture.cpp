#include "./include/gen_texture.h"
#include "./include/stb_image.h"
#include <iostream>

GLuint gen_texture(const char* filepath) {
      // Lecture de l'image
      int width, height;
      unsigned char* pixels = stbi_load(filepath, &width, &height, nullptr, 4);
      if (!pixels) {
            std::cout << "Failed to load image " << filepath << std::endl;
      }

      // Création de la texture
      GLuint texture_id;
      glGenTextures(1, &texture_id);
      glBindTexture(GL_TEXTURE_2D, texture_id);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
      glBindTexture(GL_TEXTURE_2D, 0);

      // Cleanup
      free(pixels);
      
      return texture_id;
}