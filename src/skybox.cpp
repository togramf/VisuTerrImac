#define GL_TEXTURE_CUBE_MAP_ARB             0x8513
#define GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB  0x8515
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB  0x8516
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB  0x8517
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB  0x8518
#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB  0x8519
#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB  0x851A

#define STB_IMAGE_IMPLEMENTATION
#include "./include/stb_image.h"

#include <string.h>
#include <iostream>

#include "./include/skybox.h"

GLuint gen_skybox() {
    // Test de l'extension GL_ARB_texture_cube_map
    char* extensions = (char*) glGetString(GL_EXTENSIONS); 
    if(strstr(extensions, "GL_ARB_texture_cube_map") != NULL)
    {
        // Initialisation de la CubeMap
        // Liste des faces successives pour la création des textures de CubeMap
        GLenum cube_map_target[6] = {           
            GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,
            GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,
            GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,
            GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,
            GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,
            GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB
        };
        
        // Chargement des six textures
        Img texture_image[6];
        const char* images_paths[6] = {
            "./doc/CubeMap/bluecloud_lf.jpg",
            "./doc/CubeMap/bluecloud_rt.jpg",
            "./doc/CubeMap/bluecloud_up.jpg",
            "./doc/CubeMap/bluecloud_dn.jpg",
            "./doc/CubeMap/bluecloud_ft.jpg",
            "./doc/CubeMap/bluecloud_bk.jpg",
        };
        stbi_set_flip_vertically_on_load(0);
        for (int i = 0; i < 6; ++i) {
            texture_image[i].data = stbi_load(images_paths[i], &texture_image[i].width, &texture_image[i].height, nullptr, 4);
            if (!texture_image[i].data) {
                std::cout << "Error : cannot read image" << std::endl;
            }
        }
        
        // Génération d'une texture CubeMap
        GLuint cube_map_texture_ID;
        glGenTextures(1, &cube_map_texture_ID);
        
        // Configuration de la texture
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, cube_map_texture_ID);
        
        for (int i = 0; i < 6; i++)
        {
            glTexImage2D(cube_map_target[i], 0, 3, texture_image[i].width, texture_image[i].height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_image[i].data);
        
            if (texture_image[i].data)    
            {
                free(texture_image[i].data);    
            }   
        }
        
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);

        return cube_map_texture_ID;
    } 
    else
    {
        std::cout << "ERREUR Cubemaps non supportées" << std::endl;// Autre système (six textures 2D par exemple) ou sortie de l'application ou toute autre gestion d'erreur
    }
}

void render_skybox(GLuint cube_map_texture_ID) {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_CUBE_MAP_ARB); 
    glDisable(GL_LIGHTING);
    glDepthMask(GL_FALSE);

    // Taille du cube
    float t = 1.0f;

    // Utilisation de la texture CubeMap
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, cube_map_texture_ID);
    glColor3f(1, 1, 1);
    
    // Rendu de la géométrie
    glBegin(GL_TRIANGLE_STRIP);            // X Négatif        
        glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);     
        glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
        glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
        glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);            // X Positif
        glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
        glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
        glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
        glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);            // Y Négatif    
        glTexCoord3f(-t,-t,-t); glVertex3f( t, -t,  t);
        glTexCoord3f(-t,-t,t);  glVertex3f(-t, -t,  t);
        glTexCoord3f(t, -t,-t); glVertex3f( t, -t, -t);
        glTexCoord3f(t,-t,t);   glVertex3f(-t, -t, -t);
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);            // Y Positif        
        glTexCoord3f(-t,t,-t); glVertex3f( t, t,  t);
        glTexCoord3f(t,t,-t);  glVertex3f( t, t, -t); 
        glTexCoord3f(-t,t,t);  glVertex3f(-t, t,  t);
        glTexCoord3f(t,t,t);   glVertex3f(-t, t, -t);     
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);            // Z Négatif        
        glTexCoord3f(-t,-t,-t); glVertex3f(-t,-t,-t);
        glTexCoord3f(t, -t,-t); glVertex3f(t,-t,-t);
        glTexCoord3f(-t,t,-t); glVertex3f(-t,t,-t);
        glTexCoord3f(t,t,-t); glVertex3f(t,t,-t); 
    glEnd();
    
    glBegin(GL_TRIANGLE_STRIP);            // Z Positif    
        glTexCoord3f(-t,-t,t); glVertex3f(-t,-t,t);
        glTexCoord3f(-t,t,t); glVertex3f(-t,t,t);
        glTexCoord3f(t,-t,t); glVertex3f(t,-t,t);
        glTexCoord3f(t,t,t); glVertex3f(t,t,t);     
    glEnd();

    // Réactivation de l'écriture dans le DepthBuffer
    glDepthMask(GL_TRUE);

    // Réinitialisation des états OpenGL
    glDisable(GL_TEXTURE_CUBE_MAP_ARB); 
    glEnable(GL_LIGHTING);
}