#include <cmath>
#include "./include/camera.h"


Camera::Camera()
{
	_position = createVector(0, 65.f, 0);
}


void Camera::applique_rotation() {
	glRotatef(-_tilt/M_PI*180, 1.f, 0.f, 0.f );
	glRotatef( -_pan/M_PI*180, 0.f, 1.f, 0.f);
}

void Camera::applique_translation() {
	glTranslatef( -_position.x, -_position.y, -_position.z );
}


Triangle Camera::frustum(float zfar, float fov, Vect2f image_size, Vect2f world_size) {
	return {
		world2pixels2D( {position().x, position().z}, image_size, world_size),
		world2pixels2D( addVectors(
			rotation({-zfar * tanf32(fov/2), -zfar}, -_pan),
			{ position().x, position().z }
		), image_size, world_size),
		world2pixels2D( addVectors(
			rotation({zfar * tanf32(fov/2), -zfar}, -_pan),
			{ position().x, position().z }
		), image_size, world_size),
	};
}

Vect3f Camera::position() {
	return _position;
}

float Camera::pan() {
	return _pan;
}


// Fonctions privés

void Camera::translation(Vect3f direction) {
	_position = addVectors(_position, multVector( direction, _vitesse_deplacement));
}

Vect3f Camera::forward() {
	return createVector(
		-sin(_pan),
		0,
		-cos(_pan)
	);
}

Vect3f Camera::right() {
	return createVector(
		cos(_pan),
		0,
		-sin(_pan)
	);
}

Vect3f Camera::up() {
 	return createVector(
		0,
		1,
		0
	);
}


// Fonctions mouvements caméra

void Camera::avance_tout_droit() {
	translation(forward());
}

void Camera::avance_en_arriere() {
	translation(multVector(forward(), -1));
}

void Camera::avance_gauche() {
	translation(multVector(right(), -1));
}

void Camera::avance_droite() {
	translation(right());
}

void Camera::avance_haut() {
	translation(up());
}

void Camera::avance_bas() {
	translation(multVector(up(), -1));
}

void Camera::tourne_droite() {
	_pan -= _vitesse_rotation;
}

void Camera::tourne_gauche() {
	_pan += _vitesse_rotation;
}

void Camera::tourne_haut() {
	_tilt += _vitesse_rotation;
}

void Camera::tourne_bas() {
	_tilt -= _vitesse_rotation;
}

void Camera::hauteur_fixe(float h_sol, float h_cam){
	_position.y = h_cam + h_sol;
}

void Camera::reinitialise_position(){
	_position = createVector(0, 65.f, 0);
}







