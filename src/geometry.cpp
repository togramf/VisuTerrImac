#include <GL/gl.h>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <iostream>

#include "./include/geometry.h"


Vect3f createVector(float x, float y, float z)
{
    Vect3f v;
    v.x = x;
    v.y = y;
    v.z = z;
    return v;
}


Vect2f addVectors(Vect2f v1, Vect2f v2)
{
    return { v1.x + v2.x, v1.y + v2.y };
}

Vect2f subVectors(Vect2f v1, Vect2f v2) {
    return {
        v1.x - v2.x,
        v1.y - v2.y
    };
}

Vect2f multVectors(Vect2f v1, Vect2f v2) {
    return {
        v1.x * v2.x,
        v1.y * v2.y
    };
}

Vect2f divVectors(Vect2f v1, Vect2f v2) {
    return {
        v1.x / v2.x,
        v1.y / v2.y
    };
}


Vect3f addVectors(Vect3f v1, Vect3f v2)
{
    return createVector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

Vect3f subVectors(Vect3f v1, Vect3f v2)
{   
    return createVector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

Vect3f multVector(Vect3f v, float a)
{
    if (a == 0.) 
        return createVector(0.,0.,0.);
    return createVector(a*v.x, a*v.y, a*v.z);
}

Vect3f divVector(Vect3f v, float a)
{
    if (a == 0.) 
        return v;
    return createVector(v.x/a, v.y/a, v.z/a);
}


float dot2D(Vect2f v1, Vect2f v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}

float dot3D(Vect3f v1, Vect3f v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}


float norm(Vect3f v)
{
    return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

float normSq(Vect3f v)
{
    return v.x * v.x + v.y * v.y + v.z * v.z;
}

Vect3f normalize(Vect3f v)
{
    float n = norm(v);
    return divVector(v, n);
}

Vect3f cross(Vect3f v1, Vect3f v2) 
{
    return createVector(
      v1.y * v2.z - v1.z * v2.y,
      -(v1.x * v2.z - v1.z * v2.x),
      v1.x * v2.y - v1.y * v2.x
    );
}


Vect2f rotation(Vect2f v, float angle) {
    return {
        (float) (cos(angle)*v.x - sin(angle)*v.y),
        (float) (sin(angle)*v.x + cos(angle)*v.y)
    };
}

Vect3f rotation_soleil(float angle) {
    Vect3f x_axis = {1, 0, 0};
    Vect3f y_axis = {0, 0.93, 0.34};
    return 
        addVectors(
            multVector(x_axis, cos(angle)),
            multVector(y_axis, sin(angle))
        );
}


float sign(float x) {
    return x > 0 ? 1 : -1;
}

float clamp(float x, float m, float M) {
    return std::min(std::max(x, m), M);
}

float distance(Vect3f p1, Vect3f p2)
{
    sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2)+pow(p2.z-p1.z,2));
}

float dist_to_triangle(Triangle3D triangle, Vect3f p) {
    // Thanks to https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
    Vect3f ba = subVectors(triangle.p2, triangle.p1); Vect3f pa = subVectors(p, triangle.p1);
    Vect3f cb = subVectors(triangle.p3, triangle.p2); Vect3f pb = subVectors(p, triangle.p2);
    Vect3f ac = subVectors(triangle.p1, triangle.p3); Vect3f pc = subVectors(p, triangle.p3);
    Vect3f nor = cross( ba, ac );

    return sqrt(
        (sign(dot3D(cross(ba,nor),pa)) +
        sign(dot3D(cross(cb,nor),pb)) +
        sign(dot3D(cross(ac,nor),pc))<2.0)
        ?
        std::min( std::min(
        normSq(subVectors(multVector(ba, clamp(dot3D(ba,pa)/normSq(ba),0.0,1.0)), pa)),
        normSq(subVectors(multVector(cb, clamp(dot3D(cb,pb)/normSq(cb),0.0,1.0)), pb))),
        normSq(subVectors(multVector(ac, clamp(dot3D(ac,pc)/normSq(ac),0.0,1.0)), pc)))
        :
        dot3D(nor,pa)*dot3D(nor,pa)/normSq(nor) );
}


Vect2f world2pixels2D(Vect2f v, Vect2f image_size, Vect2f world_size) {
    return 
        addVectors(
            multVectors(divVectors(v, world_size), image_size),
            multVectors(
                image_size,
                {0.5, 0.5}
            )
        );
}

Vect3f world2pixels3D(Vect3f v, Vect2f image_size, Vect2f world_size) {
    Vect2f p = {v.x, v.z};
    p = world2pixels2D(p, image_size, world_size);
    return {
        p.x,
        v.y,
        p.y
    };
}

Vect3f pixels2world(Vect3f v, Vect2f image_size, Vect2f world_size) {
    Vect2f p = {v.x, v.z};
    p = divVectors(
            multVectors(
                subVectors(
                    p,
                    multVectors(
                        image_size,
                        {0.5, 0.5}
                    )
                ),
                world_size
            ),
            image_size
    );
    return {
        p.x,
        v.y,
        p.y
    };
}


bool IsPointInSquare(Vect2f point, Square& square)
{
    return point.x<square.x2 && point.x>square.x1 && point.y<square.y2 && point.x>square.y1;
}

bool IsRightSide(Vect2f point, Vect2f p1, Vect2f p2)
{
    Vect2f normale = {p2.y-p1.y, p1.x-p2.x};
    Vect2f direction = {point.x-p1.x,point.y-p1.y};
    return dot2D(normale,direction)>0;
}

bool IsPointInTriangle(Vect2f point, Triangle& triangle)
{
    return
        IsRightSide(point, triangle.p1, triangle.p2) && 
        IsRightSide(point, triangle.p2, triangle.p3) &&
        IsRightSide(point, triangle.p3, triangle.p1);
}


bool segmentIntersection(Vect2f p1, Vect2f p2, Vect2f q1, Vect2f q2)
{
    Vect2f d1 = {p2.x-p1.x,p2.y-p1.y};
    Vect2f d2 = {q2.x-q1.x,q2.y-q1.y};
    float t1 = (1/(-d1.x*d2.y + d1.y*d2.x))*(-d2.y*(q1.x-p1.x)-d2.x*(q1.y-p1.y) );
    float t2 = (1/(-d1.x*d2.y + d1.y*d2.x))*(+d1.y*(q1.x-p1.x)+d1.x*(q1.y-p1.y) );
    return t1 > 0 && t1 < 1 && t2 > 0 && t2 < 1;
}

bool intersection(Triangle& triangle, Square& square)
{
    return 
        IsPointInSquare(triangle.p1, square) ||
        IsPointInSquare(triangle.p2, square) ||
        IsPointInSquare(triangle.p3, square) ||

        IsPointInTriangle({(float)square.x1,(float)square.y1}, triangle) ||
        IsPointInTriangle({(float)square.x2,(float)square.y2}, triangle) ||
        IsPointInTriangle({(float)square.x1,(float)square.y2}, triangle) ||
        IsPointInTriangle({(float)square.x2,(float)square.y1}, triangle) ||

        segmentIntersection(triangle.p1,triangle.p2,{(float)square.x1,(float)square.y1},{(float)square.x1,(float)square.y2}) ||
        segmentIntersection(triangle.p1,triangle.p2,{(float)square.x1,(float)square.y2},{(float)square.x2,(float)square.y1}) ||
        segmentIntersection(triangle.p1,triangle.p2,{(float)square.x2,(float)square.y1},{(float)square.x2,(float)square.y2}) ||
        segmentIntersection(triangle.p1,triangle.p2,{(float)square.x2,(float)square.y2},{(float)square.x1,(float)square.y1}) ||

        segmentIntersection(triangle.p2,triangle.p3,{(float)square.x1,(float)square.y1},{(float)square.x1,(float)square.y2}) ||
        segmentIntersection(triangle.p2,triangle.p3,{(float)square.x1,(float)square.y2},{(float)square.x2,(float)square.y1}) ||
        segmentIntersection(triangle.p2,triangle.p3,{(float)square.x2,(float)square.y1},{(float)square.x2,(float)square.y2}) ||
        segmentIntersection(triangle.p2,triangle.p3,{(float)square.x2,(float)square.y2},{(float)square.x1,(float)square.y1}) ||

        segmentIntersection(triangle.p3,triangle.p1,{(float)square.x1,(float)square.y1},{(float)square.x1,(float)square.y2}) ||
        segmentIntersection(triangle.p3,triangle.p1,{(float)square.x1,(float)square.y2},{(float)square.x2,(float)square.y1}) ||
        segmentIntersection(triangle.p3,triangle.p1,{(float)square.x2,(float)square.y1},{(float)square.x2,(float)square.y2}) ||
        segmentIntersection(triangle.p3,triangle.p1,{(float)square.x2,(float)square.y2},{(float)square.x1,(float)square.y1});
}


float conversionValue(float oldVal, float oldMax, float oldMin, float newMax, float newMin){
    return (oldVal-oldMin)*(newMax-newMin)/(oldMax-oldMin)+newMin;
}

float getHeight(int x, int y, int zmin, int zmax, Image* image)
{
    int h = image->dat[x + y * image->w];
    return conversionValue(h, image->dyn, 0, zmax, zmin);
}

float getHeightNormalise(int x, int y, Image* image)
{
    int h = image->dat[x + y * image->w];
    return conversionValue(h, image->dyn, 0, 1, 0.4);
}


void drawSquare(Square square, Image* image, int zmin, int zmax, GLuint texture, bool filled, Vect3f direction_soleil)
{
    float h = getHeightNormalise(square.x1, square.y1, image);
    if(filled) 
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture);
        glBegin(GL_TRIANGLES);
    }
    else 
    {
        glBegin(GL_LINE_STRIP);
    }
    Vect3f p11 = createVector(square.x1 , getHeight(square.x1, square.y1, zmin, zmax, image), square.y1);
    Vect3f p21 = createVector(square.x2 , getHeight(square.x2, square.y1, zmin, zmax, image), square.y1);
    Vect3f p12 = createVector(square.x1 , getHeight(square.x1, square.y2, zmin, zmax, image), square.y2);
    Vect3f p22 = createVector(square.x2 , getHeight(square.x2, square.y2, zmin, zmax, image), square.y2);
    
    Vect3f normale1 = normalize(
        cross(
            subVectors(p21, p11),
            subVectors(p12, p11)
        )
    );    
    Vect3f normale2 = normalize(
        cross(
            subVectors(p12, p22),
            subVectors(p21, p22)
        )
    );
    float diffuse1 = -std::min(0.f, dot3D(normale1, direction_soleil));
    float diffuse2 = -std::min(0.f, dot3D(normale2, direction_soleil));

    glColor3f(h * diffuse1, h * diffuse1, h * diffuse1);
    glTexCoord2f(0.f, 1.f); glVertex3f(p12.x, p12.y, p12.z);
    glTexCoord2f(0.f, 0.f); glVertex3f(p11.x, p11.y, p11.z);
    glTexCoord2f(1.f, 0.f); glVertex3f(p21.x, p21.y, p21.z);
    

    glColor3f(h * diffuse2, h * diffuse2, h * diffuse2);
    glTexCoord2f(0.f, 1.f); glVertex3f(p12.x, p12.y, p12.z);
    glTexCoord2f(1.f, 1.f); glVertex3f(p22.x, p22.y, p22.z);
    glTexCoord2f(1.f, 0.f); glVertex3f(p21.x, p21.y, p21.z);
    

    glEnd();
    if(filled) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
}

void drawTerrain(Node* node, Triangle cameraFrustum, Vect3f positionCam, Image* image, int zmin, int zmax, GLuint texture, bool filled,  Vect3f direction_soleil, Vect2f image_size, Vect2f world_size)
{
    if (intersection(cameraFrustum , node->square))
    {
        Vect3f p11 = createVector(node->square.x1 , getHeight(node->square.x1, node->square.y1, zmin, zmax, image), node->square.y1);
        Vect3f p21 = createVector(node->square.x2 , getHeight(node->square.x2, node->square.y1, zmin, zmax, image), node->square.y1);
        Vect3f p12 = createVector(node->square.x1 , getHeight(node->square.x1, node->square.y2, zmin, zmax, image), node->square.y2);
        Vect3f p22 = createVector(node->square.x2 , getHeight(node->square.x2, node->square.y2, zmin, zmax, image), node->square.y2);
        Triangle3D trg1 = {
            pixels2world(p11, image_size, world_size),
            pixels2world(p21, image_size, world_size),
            pixels2world(p12, image_size, world_size)
        };
        Triangle3D trg2 = {
            pixels2world(p22, image_size, world_size),
            pixels2world(p21, image_size, world_size),
            pixels2world(p12, image_size, world_size)
        };
        float distanceCam = std::min(
            dist_to_triangle(trg1, positionCam),
            dist_to_triangle(trg2, positionCam)
        );

        int taille = node->square.x2 - node->square.x1;
        bool is_leaf = node->hautg == nullptr; 
        bool early_exit_because_of_lod = log(taille) < 0.007*distanceCam;
        if (is_leaf || early_exit_because_of_lod)
        {
            drawSquare(node->square, image, zmin, zmax, texture, filled, direction_soleil);
        }
        else
        {
            drawTerrain(node->hautg, cameraFrustum, positionCam, image, zmin, zmax, texture, filled, direction_soleil, image_size, world_size);
            drawTerrain(node->hautd, cameraFrustum, positionCam, image, zmin, zmax, texture, filled, direction_soleil, image_size, world_size);
            drawTerrain(node->basg,  cameraFrustum, positionCam, image, zmin, zmax, texture, filled, direction_soleil, image_size, world_size);
            drawTerrain(node->basd,  cameraFrustum, positionCam, image, zmin, zmax, texture, filled, direction_soleil, image_size, world_size);
        }
    }
}

void drawObjet(GLuint texture_arbre,int zmin, int zmax,Image* image,float pan,float x,float y,float obj_hauteur,Vect3f direction_soleil){
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND); 
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA); 
    glBindTexture(GL_TEXTURE_2D, texture_arbre);
    
    float z = getHeight(x,y,zmin,zmax,image)+obj_hauteur-0.5;
    float diffuse1 = -std::min(0.f, dot3D({-0, -0.93, -0.34}, direction_soleil));
    glPushMatrix();
        glTranslatef(x,z,y);
        glRotatef(pan/M_PI*180,0.f,1.0f,0.f);
        glColor3f(diffuse1,diffuse1,diffuse1);
        glBegin(GL_QUADS);
    
        glTexCoord2f(0, 1);
        glVertex2f(-obj_hauteur, -obj_hauteur);
    
        glTexCoord2f(1, 1);
        glVertex2f(obj_hauteur, -obj_hauteur);
    
        glTexCoord2f(1, 0);
        glVertex2f(obj_hauteur, obj_hauteur);
    
        glTexCoord2f(0, 0);
        glVertex2f(-obj_hauteur, obj_hauteur);

        glEnd();

    glPopMatrix();

    glBindTexture(GL_TEXTURE_2D, 0);
    
    glDisable(GL_TEXTURE_2D);
    
}
