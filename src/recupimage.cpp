#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include <assert.h>

#include "./include/recupimage.h"

int recupParamMap(char* argv, char* path, float* xsize, float* ysize, float* zmin, float* zmax, float* znear, float* zfar, float* fov){
    FILE* terrain = NULL; 
    char ligne[256]="";

    terrain = fopen(argv, "r+");
    if (terrain == NULL){
        return 0;
    }

    fgets(ligne, 256, terrain); 
    sscanf(ligne,"%s", path);
    fgets(ligne, 256, terrain); 
    sscanf(ligne,"%f %f %f %f", xsize, ysize, zmin, zmax);
    fgets(ligne, 256, terrain); 
    sscanf(ligne,"%f %f %f", znear, zfar, fov);
    fclose(terrain); 

    return 1;
}

Image* newImage(int w, int h, int d){
    Image* I = new Image();
    I->w = w;
    I->h = h;
    I->dyn = d;
    I->dat.reserve(w*h);
    return I;
}

int getPixel (Image* I, int i, int j){
    assert(I && i>=0 && i<I->w && j>=0 && j<I->h);
    return I->dat[I->w*j+i];
}

Image* loadImage (char* fichier) {
    std::ifstream f(fichier);
    if (!f.is_open()){
        printf("Erreur ouverture fichier\n");
        return NULL;
    } else 
        printf("fichier ouvert \n");

    int i, j, max;
    std::string ligne;
    bool is_header_read = false;
    bool is_size_read = false;
    bool is_color_depth_read = false;
    Image* image;
    int w, h, d;
    
    while(std::getline(f, ligne)) {
        if (ligne.find('#') == std::string::npos) {
            if (!is_header_read) {
                is_header_read = true; 
            }
            else if (!is_size_read) {
                is_size_read = true;
                std::string width_string = ligne.substr(0, ligne.find(' ')); 
                std::string heigth_string = ligne.substr(ligne.find(' ')+1,ligne.length()); 
                w = std::stoi(width_string);
                h = std::stoi(heigth_string);
            }
            else if (!is_color_depth_read) {
                is_color_depth_read = true;
                d = std::stoi(ligne);
                image = newImage(w, h, d);
            }
            else {
                image->dat.push_back(std::stoi(ligne));
            }
        }
    }

    return image;
}
