# VisuTerrImac
Projet de SI - ALGO, IMAC S2 

Lien du Notion : https://www.notion.so/ALGO-SI-Projet-VisuTerrImac-2d7282719e1e4e03b67a62c7641871f0 

# Compiler
make

# Lancer le projet
bin/main.out doc/terrain.timac

# Les touches 
Translation 
	avant : z
	arrière : s
	droite : d
	gauche : q
	haut : e
	bas : barre espace
Rotation
	droite : fleche de droite
	gauche : fleche de gauche
	haut : fleche du haut
	bas : fleche du bas

Mode visualistation (hauteur fixe) : v
Levé/couché de soleil : l ou m
Affichage fil de fer : f
Retour au centre de la map : r

Objet 
	ajoute un arbre : 1
	enlève un arbre : 2
	ajoute un panda : 3
	enlève un panda : 4
	ajoute une chèvre : 5 attention il n'y en a qu'une !
	enlèvre une chèvre : 6
	ajoute tous les objets : 7
	enlève tous les objets : 8

# Ce qu'il reste à faire
hé mais c'est fini c'est une dinguerie !!

# Choix / Problèmes à expliquer dans le rapport
- Plan (O,x,y) avec z la hauteur du terrain
- QuadTree marche uniquement quand l'image est carré et puissance de 2
- Amélioration : ajouter d'autres objets
- Réparer les cracks
- Couleurs des QuadTree dans le fil de fer
- Diffusion texture avec soleil